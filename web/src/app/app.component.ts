import { Component } from '@angular/core';
import { PostApi, Post } from './shared/sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PostApi]
})
export class AppComponent {
  title = 'Micro Blog!';
  private posts: Post[];
  private post: Post = new Post();
  
  constructor(private postApi: PostApi){
    this.getPosts();
  }
  
  getPost(id: any): void{
    this.postApi.findById(id)
      .subscribe((post: Post) => {
        this.post = post;
        return this.post;
      })
  }
  
  getPosts(){
    this.postApi.find().subscribe((r: Post[]) => {
      this.posts = r;
      return this.posts; 
    });
  }
  
  savePost(post: Post){
    if(post.id){
      this.postApi.replaceById(post.id, post).subscribe((r) => {
        this.getPosts();
        return r;
      });
    } else {
      this.postApi.create(post).subscribe((r: Post) => {
        this.getPosts();
        return r;
      });
    }
    
  }
  
  
  deletePost(post: Post): void {
    this.postApi.deleteById(post.id).subscribe((r) => {
      this.getPosts();
    });
  }

  selectPost(post: Post): void {
    this.post = {...post};
  }
 
}
