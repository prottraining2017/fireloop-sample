var should    = require('chai').should();
var supertest = require('supertest');
var api       = supertest('http://localhost:3000/api');

describe('Post unit tests:', () => {
    it('Should create a Post instance', (done: Function) => {
        api.post('/posts').send({
            title: 'test',
            description: 'test'
        }).expect(200, done);
    });
});
